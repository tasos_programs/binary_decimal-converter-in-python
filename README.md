# Binary/Decimal Converter In Python

My own implementation to convert a binary or decimal number to the opposite equivalent.
There are no command line arguments. Just use
```bash
python convert.py
```

# Requirements
The GUI Version requires appJar.
Use [pip](https://pip.pypa.io/en/stable/) to install it.
```bash
pip install appJar
```
It also depends on base program *convert. py* so make sure it's in the same directory.
# Functions

 - Convert a **binary** number to **decimal**
 - Convert a **decimal** number to **binary**
