# Expected Results:
# Binary: Input: 11, Output: 3
# Decimal: Input: 15, Output: 1111

def CheckIfNegative(num : int) -> bool:
    return True if num < 0 else False

def AddAppropriateBit(isNegative : bool, result : str) -> str:
    # Change first index of result depending if positive or negative number
    # Obviously, we're adding to the end since we need to reverse the string later.
    result += '-' if isNegative else ''
    return result[::-1] # [::-1] reverses the string

def DecimalToBinary(num : int) -> str:
    isNegative = CheckIfNegative(num)
    result = ""
    # Check if num is negative
    if isNegative:
        num = abs(num)

    while num > 0:
        result += str(num % 2)
        num //= 2

    result = AddAppropriateBit(isNegative,result)
    return result

def BinaryToDecimal(num) -> int:
    result = 0
    power = len(num) - 1
    for index in range(len(num)):
        # for every char, check if 0 or 1. Add by 2^index
        if IsOne(num[index]):
            result += pow(2,power)
        power-=1
    return result

def IsOne(num) -> bool:
    return True if int(num) else False

def GetInput() -> str:
    return input("Enter a Number: ")

def ValidateInput(num):
    # 0 is 0 in both (can't divide by 0 for binary to decimal)
    try:
        num = int(num)
    except ValueError:
        print("Not a number.")
        return False
    if num == 0:
        print ("0 is 0 in both numeric systems.")
        return False
    return True

def GetConversion() -> int:
    return int(input("0 = To Binary\n1 = To Decimal\nEnter: "))

def main():
    num = GetInput()
    if not ValidateInput(num):
        return

    if (GetConversion()):
        print(num,"binary is",BinaryToDecimal(num),"in decimal")       
    else:
        print(num,"in Decimal is",DecimalToBinary(int(num)),"in binary")

    return num

if __name__ == "__main__":
    main()
    
