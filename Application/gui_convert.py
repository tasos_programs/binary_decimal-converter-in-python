try:
    from convert import BinaryToDecimal, DecimalToBinary
    from appJar import gui
except ModuleNotFoundError:
    print("A module was not found.\nVerify that 'convert.py' is in the same directory\nVerify that appJar is installed. (pip install appJar)")
    exit()
    
def SelectedConvertToDecimal() -> bool:
    if app.getRadioButton("UNIT") == "To Decimal":
        return True
    return False

def Convert() -> None:
    """Function to call conversion functions from module 'convert'"""
    num = app.getEntry("Convert Number") # Get number
    # Update our entry to reflect the result of the conversion.
    if (SelectedConvertToDecimal()): 
        app.setEntry("Result",BinaryToDecimal(num))
    else:
        app.setEntry("Result",DecimalToBinary(int(num)))

def InitApp() -> None:
    """Function to initialize our program. Adds everything to the ui"""
    app.addLabelEntry("Convert Number")
    app.addRadioButton("UNIT","To Binary",row=1,column=0)
    app.addRadioButton("UNIT","To Decimal",row=2,column=0)  
    app.addButton("Convert",Convert,row=3,column=0)
    app.addLabelEntry("Result",row=4,column=0,colspan=2)
    app.go()

def main():
    InitApp()

if __name__ == "__main__":
    app = gui() # appJar module
    main()
